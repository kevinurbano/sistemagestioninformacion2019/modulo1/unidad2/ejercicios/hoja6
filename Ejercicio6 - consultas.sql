﻿USE m1u2h6;

/* Ejercicio 1 */
CREATE OR REPLACE VIEW ejer1 AS
  SELECT 
    c.`CÓDIGO CLIENTE`, p.`NÚMERO DE PEDIDO`, c.EMPRESA, c.POBLACIÓN
    FROM 
      clientes c
    JOIN 
      pedidos p 
    ON c.`CÓDIGO CLIENTE` = p.`CÓDIGO CLIENTE`
    WHERE
      c.POBLACIÓN IN ('Madrid','Barcelona','Zaragoza');

/* Ejercicio 2  */
CREATE OR REPLACE VIEW CONSULTA1OPTIMIZADA AS
  SELECT 
    c.`CÓDIGO CLIENTE`,
    c.EMPRESA,
    c.POBLACIÓN,
    p.`NÚMERO DE PEDIDO` 
    FROM 
      pedidos p
    JOIN
      (
        SELECT 
          c.`CÓDIGO CLIENTE`,
          c.EMPRESA,
          c.POBLACIÓN
        FROM 
          clientes c
        WHERE 
          c.POBLACIÓN IN ('Madrid','Barcelona','Zaragoza')
      ) c1 USING(`CÓDIGO CLIENTE`);

/* Ejercicio 3 */
-- Da error por que en las vistas no pueden tener subconsultas

/* Ejercicio 4 */
-- Creando subconsulta
CREATE OR REPLACE VIEW SubconsultaConsulta1Optimizada AS
  SELECT 
    c.`CÓDIGO CLIENTE`,
    c.EMPRESA,
    c.POBLACIÓN
  FROM 
    clientes c
  WHERE 
    c.POBLACIÓN IN ('Madrid','Barcelona','Zaragoza');

CREATE OR REPLACE VIEW CONSULTA1OPTIMIZADA AS
  SELECT 
    c1.`CÓDIGO CLIENTE`,
    c1.EMPRESA,
    c1.POBLACIÓN,
    p.`NÚMERO DE PEDIDO` 
    FROM 
      pedidos p
    JOIN
      SubconsultaConsulta1Optimizada c1 USING(`CÓDIGO CLIENTE`);
   

/* 
    Ejercicio 5 
    Crear una vista que nos permita realizar una consulta 
    que muestre que clientes han realizado algún pedido en el año 2002 
    y su forma de pago fue tarjeta. 
    Utilizamos los campos EMPRESA y POBLACIÓN de la tabla CLIENTES, y NÚMERO DE PEDIDO, 
    FECHA DE PEDIDO y FORMA DE PAGO de la tabla PEDIDOS.
    Ordenar los registros por el campo FECHA DE PEDIDO de forma ascendente. 
    Guardamos la vista con el nombre CONSULTA 2. 
    La vista debe tener la consulta optimizada.
    Si necesitas crear más de una vista colócalas el nombre que desees. 
*/

/* Ejercicio 6 */
-- C1
CREATE OR REPLACE VIEW Subconsulta1Consulta2 AS
  SELECT 
    p.`NÚMERO DE PEDIDO`,p.`FECHA DE PEDIDO`,p.`FORMA DE PAGO`,p.`CÓDIGO CLIENTE`
    FROM 
      pedidos p
    WHERE
      YEAR(p.`FECHA DE PEDIDO`)=2002 AND p.`FORMA DE PAGO`='tarjeta';


CREATE OR REPLACE VIEW Consulta2 AS
  SELECT c.EMPRESA,c.POBLACIÓN,c1.`NÚMERO DE PEDIDO`,c1.`FECHA DE PEDIDO`,c1.`FORMA DE PAGO`        
    FROM 
      clientes c
    JOIN
      Subconsulta1Consulta2 c1 USING(`CÓDIGO CLIENTE`)
    ORDER BY
      c1.`FECHA DE PEDIDO` ASC;

/* Ejercicio 7 */
-- C1
CREATE OR REPLACE VIEW Subconsulta1Consulta3 AS
  SELECT
    p.`CÓDIGO CLIENTE`,COUNT(*) nPedidos 
    FROM 
      pedidos p
    GROUP BY
      p.`CÓDIGO CLIENTE`;

-- C2
CREATE OR REPLACE VIEW Subconsulta2Consulta3 AS
  SELECT 
    MAX(nPedidos) maximo
    FROM 
      Subconsulta1Consulta3 c1;

-- C3
CREATE OR REPLACE VIEW Subconsulta3Consulta3 AS
  SELECT 
    c1.`CÓDIGO CLIENTE`
    FROM
      subconsulta1consulta3 c1
    JOIN
      subconsulta2consulta3 c2 ON c1.nPedidos=c2.maximo; 

-- Final     
CREATE OR REPLACE VIEW consulta3 AS
  SELECT 
    c.`CÓDIGO CLIENTE`,c.TELÉFONO
    FROM 
      clientes c
    JOIN
      Subconsulta3Consulta3 sc USING (`CÓDIGO CLIENTE`);



        

